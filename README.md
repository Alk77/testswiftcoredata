# TestSwiftCoreData


TestSwiftCoreData is a project created with educational purpose. In this project implemented the client-server interaction using Alamofire framework, and CoreData as an entities storage. I also wanted to find a framework that would automatically parse REST objects and put them into CoreData.

Main features of application:
- allows to user to log in and shows the user info;
- allows auto-login during the day since the last visit of the application;
- creation the new account by entering user info;
- restoring the password by sending the new random one to user email;
- allows to user to log out; all stored user info is cleared;
- handle two roles: the Customer and the Cleaner.

Test account: customer@xcodes.net, password: qwerty123
Or: cleaner@xcodes.net, password: qwerty123
