//
//  Constants.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 12/23/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

// MARK: - business logic constants
let minNumberOfBedrooms = 0
let maxNumberOfBedrooms = 12
let minNumberOfBathrooms = 1
let maxNumberOfBathrooms = 10
let minNumberOfKitchens = 1
let maxNumberOfKitchens = 3
let minNumberOfCommonAreas = 1
let maxNumberOfCommonAreas = 5

// MARK: - other constants
let defaultLatitude = 47.839532
let defaultLongitude = 35.140565
let expiresTokenValue: Int32 = 86400
let googleMapsAPIKey = "AIzaSyBOKPdcVwOS2BO_HDe-VUcHeoUVsm_l9XA"
