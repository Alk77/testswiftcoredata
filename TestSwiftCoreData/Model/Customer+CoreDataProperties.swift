//
//  Customer+CoreDataProperties.swift
//  
//
//  Created by Alex Korabl'ov on 9/11/18.
//
//

import Foundation
import CoreData


extension Customer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Customer> {
        return NSFetchRequest<Customer>(entityName: "Customer")
    }

    @NSManaged public var customerId: Int64
    @NSManaged public var favouriteCleaners: NSObject?
    @NSManaged public var house: [House]?
    @NSManaged public var profile: Profile?

}
