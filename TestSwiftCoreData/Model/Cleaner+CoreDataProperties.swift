//
//  Cleaner+CoreDataProperties.swift
//
//  Created by Alex Korabl'ov on 12/28/18.
//
//

import Foundation
import CoreData


extension Cleaner {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cleaner> {
        return NSFetchRequest<Cleaner>(entityName: "Cleaner")
    }

    @NSManaged public var cleanerID: Int64
    @NSManaged public var favouriteCustomers: NSObject?
    @NSManaged public var profile: Profile?

}
