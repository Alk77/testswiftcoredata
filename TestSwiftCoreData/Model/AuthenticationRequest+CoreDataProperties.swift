//
//  AuthenticationRequest+CoreDataProperties.swift
//  
//
//  Created by Alex Korabl'ov on 9/11/18.
//
//

import Foundation
import CoreData


extension AuthenticationRequest {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AuthenticationRequest> {
        return NSFetchRequest<AuthenticationRequest>(entityName: "AuthenticationRequest")
    }

    @NSManaged public var authentication: Authentication?
    @NSManaged public var sessionConfig: SessionConfig?

}
