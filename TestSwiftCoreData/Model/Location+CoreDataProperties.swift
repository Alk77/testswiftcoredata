//
//  Location+CoreDataProperties.swift
//  
//
//  Created by Alex Korabl'ov on 9/11/18.
//
//

import Foundation
import CoreData


extension Location {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }

    @NSManaged public var lat: Float
    @NSManaged public var lng: Float
    @NSManaged public var location: House?

}
