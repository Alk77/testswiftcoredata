//
//  Authentication+CoreDataProperties.swift
//  
//
//  Created by Alex Korabl'ov on 9/11/18.
//
//

import Foundation
import CoreData


extension Authentication {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Authentication> {
        return NSFetchRequest<Authentication>(entityName: "Authentication")
    }

    @NSManaged public var email: String
    @NSManaged public var fbToken: String?
    @NSManaged public var password: String?
    @NSManaged public var type: String
    @NSManaged public var authentication: AuthenticationRequest

}
