//
//  House+CoreDataProperties.swift
//  
//
//  Created by Alex Korabl'ov on 9/11/18.
//
//

import Foundation
import CoreData


extension House {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<House> {
        return NSFetchRequest<House>(entityName: "House")
    }

    @NSManaged public var address: String?
    @NSManaged public var bathrooms: Int32
    @NSManaged public var bedrooms: Int32
    @NSManaged public var flat: String?
    @NSManaged public var houseId: Int64
    @NSManaged public var kitchens: Int32
    @NSManaged public var otherRooms: Int32
    @NSManaged public var type: String?
    @NSManaged public var house: Customer?
    @NSManaged public var location: Location?

}
