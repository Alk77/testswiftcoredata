//
//  SessionConfig+CoreDataProperties.swift
//  
//
//  Created by Alex Korabl'ov on 9/11/18.
//
//

import Foundation
import CoreData


extension SessionConfig {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SessionConfig> {
        return NSFetchRequest<SessionConfig>(entityName: "SessionConfig")
    }

    @NSManaged public var expiresIn: Int32
    @NSManaged public var platform: String?
    @NSManaged public var pushToken: String?
    @NSManaged public var version: String?
    @NSManaged public var sessionConfig: AuthenticationRequest?

}
