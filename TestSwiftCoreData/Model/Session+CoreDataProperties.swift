//
//  Session+CoreDataProperties.swift
//  
//
//  Created by Alex Korabl'ov on 9/11/18.
//
//

import Foundation
import CoreData


extension Session {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Session> {
        return NSFetchRequest<Session>(entityName: "Session")
    }

    @NSManaged public var authToken: String?
    @NSManaged public var cleanerId: Int64
    @NSManaged public var createdAt: Int32
    @NSManaged public var customerId: Int64
    @NSManaged public var expiresIn: Int32
    @NSManaged public var isEmailVerified: Bool
    @NSManaged public var isNewUser: Bool
    @NSManaged public var managerId: Int64
    @NSManaged public var profileId: Int64
    @NSManaged public var refreshToken: String?
    @NSManaged public var scToken: String?

}
