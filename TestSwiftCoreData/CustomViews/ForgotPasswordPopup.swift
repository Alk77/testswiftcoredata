//
//  ForgotPasswordPopup.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 12/25/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

protocol ForgotPasswordPopupDelegate : class {
	func didEnterEmail(_ email: String, inView: ForgotPasswordPopup)
}

class ForgotPasswordPopup: UIViewController, UITextFieldDelegate {
	
	weak var delegate: ForgotPasswordPopupDelegate?
	
	@IBOutlet weak var forgotPasswordPopup: UIControl!
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var errorView: UIView!
	@IBOutlet weak var errorLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
		self.forgotPasswordPopup.layer.cornerRadius = 5.0
		self.forgotPasswordPopup.layer.shadowOpacity = 0.8
		self.forgotPasswordPopup.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
		
		self.errorView.layer.cornerRadius = 5.0
		self.errorView.layer.shadowOpacity = 0.8
		self.errorView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
		self.errorView.isHidden = true
		self.errorView.isUserInteractionEnabled = false
		
		self.emailTextField.attributedPlaceholder = NSAttributedString.init(string: "Your Email:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
	
	// MARK: - button handlers
    
	@IBAction func backgroundTapped(_ sender: UIControl) {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func popupTapped(_ sender: UIControl) {
		self.view.endEditing(true)
	}
	
	@IBAction func sendPressed(_ sender: UIButton) {
		if !self.checkEmail(email: self.emailTextField.text!) {
			self.emailTextField.text! = ""
			self.emailTextField.attributedPlaceholder = NSAttributedString.init(string: "Your Email:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
			self.errorLabel.text! = "Wrong email. Please enter the valid email."
			self.errorView.isHidden = false
			self.errorView.isUserInteractionEnabled = true
		}
		else {
			self.dismiss(animated: true) {
				self.delegate?.didEnterEmail(self.emailTextField.text!, inView: self)
			}
		}
	}
	
	@IBAction func okErrorPressed(_ sender: UIButton) {
		self.errorView.isUserInteractionEnabled = false
		self.errorView.isHidden = true
	}
	
	// MARK: - text field delegates
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		textField.placeholder = ""
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		self.view.endEditing(true)
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField.text! == "" {
			textField.attributedPlaceholder = NSAttributedString.init(string: "Your Email:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		}
	}
	
	// MARK: - service methods
	
	fileprivate func checkEmail(email:String) -> Bool {
		let placeholder = "^[a-z0-9A-Z]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
		let pattern = String(format: placeholder, email)
		
		do {
			let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
			let range = NSMakeRange(0, email.count)
			return regex.numberOfMatches(in: email, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range) > 0
		}
		catch let error {
			print("invalid regex: \(error.localizedDescription)")
			return false
		}
	}

}
