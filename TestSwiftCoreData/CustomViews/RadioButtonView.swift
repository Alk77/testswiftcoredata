//
//  RadioButtonView.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 12/24/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

protocol RadioButtonViewDelegate: class {
	func choosedRadioButton(view: RadioButtonView, with number: Int)
}

class RadioButtonView: UIView {

	weak var delegate: RadioButtonViewDelegate?
	
	var selectedItem: Int?
	fileprivate var labels: [String]
	fileprivate var selectedImage: UIImage?
	fileprivate var unselectedImage: UIImage?
	
	init?(frame: CGRect, buttonLabels: [String], selectedImage: UIImage, unselectedImage: UIImage, titleColor: UIColor) {
		let padding = 8.0;
		if Double(frame.size.height) - 2.0 * padding < Double(buttonLabels.count) * 16.0
		{
			fatalError("too small field height for the radio button view")
		}
		self.labels = buttonLabels
		self.selectedImage = selectedImage
		self.unselectedImage = unselectedImage
		self.selectedItem = 0
		
		super.init(frame: frame)
		
		self.backgroundColor = .clear
		var currentHeight = padding
		let buttonHeight = (Double(self.frame.size.height) - 2.0 * padding) / Double(labels.count)
		let fontSize: CGFloat = UIDevice.current.userInterfaceIdiom == .phone ? 18.0 : 22.0
		for i in 0..<labels.count {
			let button = UIButton.init(frame: CGRect(x: padding, y: currentHeight, width: Double(self.frame.size.width - 16.0), height: buttonHeight))
			button.setImage(selectedImage, for: .selected)
			button.setImage(unselectedImage, for: .normal)
			button.imageView?.contentMode = .scaleAspectFit
			button.setTitle(labels[i], for: .normal)
			button.setTitleColor(titleColor, for: .normal)
			button.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 8.0, bottom: 0.0, right: 0.0)
			button.titleLabel?.font = UIFont.init(name: "LATO-REGULAR", size: fontSize)
			button.contentHorizontalAlignment = .left
			button.tag = i + 1
			button.addTarget(self, action: #selector(RadioButtonView.selectItem(_:)), for: .touchUpInside)
			self.addSubview(button)
			button.isSelected = (i == self.selectedItem)
			currentHeight += buttonHeight
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func updateItem(_ selected: Int) {
		self.selectedItem = selected;
		for i in 0..<labels.count {
			if let button = self.viewWithTag(i + 1) as? UIButton {
				button.isSelected = (button.tag == selected + 1)
			}
		}
	}
	
	@objc func selectItem(_ sender: UIButton) {
		updateItem(sender.tag - 1)
		self.delegate?.choosedRadioButton(view: self, with: self.selectedItem!)
	}
}
