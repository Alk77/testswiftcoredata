//
//  StepperView.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 12/25/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

protocol StepperViewDelegate : class {
	func didChangeValue(_ value: Int, in view:StepperView)
}

class StepperView: UIView {

	weak var delegate: StepperViewDelegate?
	fileprivate var amountLabel = UILabel()
	fileprivate var range: ClosedRange<Int>?
	fileprivate var currentValue: Int
	fileprivate var isLocked = false
	
	init(frame: CGRect, background: UIImage, plusImage: UIImage, minusImage: UIImage, range: ClosedRange<Int>) {
		self.range = range
		self.currentValue = range.first!
		var backRect, plusRect, minusRect: CGRect
		var buttonFont: UIFont
		if UIDevice.current.userInterfaceIdiom == .phone {
			backRect = CGRect(x: frame.size.width * 0.1, y: frame.size.height * 0.2, width: frame.size.width * 0.8, height: frame.size.height * 0.6)
			plusRect = CGRect(x: frame.size.width * 0.9 - frame.size.height * 0.6 - 8.0, y: frame.size.height * 0.2, width: frame.size.height * 0.6, height: frame.size.height * 0.6)
			minusRect = CGRect(x: frame.size.width * 0.1 + 8.0, y: frame.size.height * 0.2, width: frame.size.height * 0.6, height: frame.size.height * 0.6)
			buttonFont = UIFont(name: "LATO-REGULAR", size: 20) ?? UIFont.systemFont(ofSize: 20)
		}
		else {
			backRect = CGRect(x: frame.size.width * 0.2, y: frame.size.height * 0.2, width: frame.size.width * 0.6, height: frame.size.height * 0.6)
			plusRect = CGRect(x: frame.size.width * 0.8 - frame.size.height * 0.6 - 8.0, y: frame.size.height * 0.2, width: frame.size.height * 0.6, height: frame.size.height * 0.6)
			minusRect = CGRect(x: frame.size.width * 0.2 + 8.0, y: frame.size.height * 0.2, width: frame.size.height * 0.6, height: frame.size.height * 0.6)
			buttonFont = UIFont(name: "LATO-REGULAR", size: 24) ?? UIFont.systemFont(ofSize: 24)
		}
		super.init(frame: frame)
		
		self.backgroundColor = .clear
		
		let back = UIView.init(frame: backRect)
		back.backgroundColor = UIColor(red: 24.0/255.0, green: 70.0/255.0, blue: 114.0/255.0, alpha: 1.0)
		back.layer.cornerRadius = back.bounds.size.height * 0.5
		self.addSubview(back)
		
		let plusButton = UIButton.init(frame: plusRect)
		plusButton.setImage(plusImage, for: .normal)
		plusButton.imageView?.contentMode = .scaleAspectFit
		plusButton.addTarget(self, action: #selector(plusPressed(_:)), for: .touchUpInside)
		self.addSubview(plusButton)
		
		let minusButton = UIButton(frame: minusRect)
		minusButton.setImage(minusImage, for: .normal)
		minusButton.imageView?.contentMode = .scaleAspectFit
		minusButton.addTarget(self, action: #selector(minusPressed(_:)), for: .touchUpInside)
		self.addSubview(minusButton)
		
		amountLabel = UILabel.init(frame: CGRect(x: frame.size.width * 0.4, y: frame.size.height * 0.2, width: frame.size.width * 0.2, height: frame.size.height * 0.6))
		amountLabel.text = String(self.currentValue)
		amountLabel.textColor = self.isLocked ? .gray : .white
		amountLabel.font = buttonFont
		amountLabel.textAlignment = .center
		self.addSubview(amountLabel)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	@objc fileprivate func plusPressed(_ sender: UIButton) {
		if self.currentValue >= (self.range?.last)! {
			return
		}
		self.currentValue += 1
		amountLabel.text = String(self.currentValue)
		self.delegate?.didChangeValue(self.currentValue, in: self)
	}
	
	@objc fileprivate func minusPressed(_ sender: UIButton) {
		if self.currentValue <= (self.range?.first)! {
			return
		}
		self.currentValue -= 1
		amountLabel.text = String(self.currentValue)
		self.delegate?.didChangeValue(self.currentValue, in: self)
	}
	
	func updateStepperWith(value: Int) -> Bool {
		if value > (self.range?.last)! || value < (self.range?.first)! {
			return false
		}
		self.currentValue = value
		amountLabel.text = String(self.currentValue)
		return true
	}
	
	func updateViewInState(locked: Bool) {
		self.isLocked = locked
		amountLabel.textColor = self.isLocked ? .gray : .white
	}
}
