//
//  PropertyDetailsViewController.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlacePicker

class PropertyDetailsViewController: UIViewController, IPropertyDetailsProtocol, CLLocationManagerDelegate, GMSPlacePickerViewControllerDelegate, UITextFieldDelegate, StepperViewDelegate, RadioButtonViewDelegate {
	
	var userProfile: [String: Any] = [:]
	var isRegisteredWithFB = false
    
    @IBOutlet weak var transparentPropertyTypeView: UIView!
    @IBOutlet weak var transparentAddressView: UIView!
    @IBOutlet weak var transparentBedroomsNumberView: UIView!
    @IBOutlet weak var transparentBathroomsNumberView: UIView!
    @IBOutlet weak var transparentKitchensNumberView: UIView!
    @IBOutlet weak var transparentOtherNumberView: UIView!
	@IBOutlet weak var propertyContainer: UIView!
	@IBOutlet weak var addressTextView: UITextView!
	@IBOutlet weak var mapLocation: UIImageView!
	@IBOutlet weak var flatNumberTextField: UITextField!
	
	fileprivate let presenter = PropertyDetailsPresenter()
	fileprivate let propertyTypeRadioButtonTag = 100
	fileprivate let numberOfBedroomsViewTag = 101
	fileprivate let numberOfBathroomsViewTag = 102
	fileprivate let numberOfKitchensViewTag = 103
	fileprivate let numberOfCommonAreasViewTag = 104
	fileprivate let otherContainerViewTag = 105
	
    fileprivate static let toFinishScreenSegue = "fromPropertyDetailsToFinishSegue"
	
	fileprivate let propertyTypes = ["House", "Apartment", "Office building"]
	fileprivate var selectedPropertyType = 0
	fileprivate var numberOfBedrooms = minNumberOfBedrooms
	fileprivate var numberOfBathrooms = minNumberOfBathrooms
	fileprivate var numberOfKitchens = minNumberOfKitchens
	fileprivate var numberOfCommonAreas = minNumberOfCommonAreas
	fileprivate let locationManager = CLLocationManager()
	fileprivate var userLocation = CLLocationCoordinate2DMake(defaultLatitude, defaultLongitude)
	
	// MARK: - view controller life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
		
		initLocation()
        initUI()
    }
	
	fileprivate func initLocation() {
		GMSServices.provideAPIKey(googleMapsAPIKey)
		GMSPlacesClient.provideAPIKey(googleMapsAPIKey)
		
		locationManager.requestWhenInUseAuthorization()
		findUserLocation()
	}
	
	fileprivate func initUI() {
		// transparent navigation bar
		navigationController?.navigationBar.shadowImage = UIImage.init()
		navigationController?.navigationBar.isTranslucent = true
		navigationController?.view.backgroundColor = UIColor.clear
		navigationController?.navigationBar.backgroundColor = UIColor.clear
		
		// background
		let backGradient:CAGradientLayer = CAGradientLayer.init()
		backGradient.frame = view.bounds
		backGradient.colors = [UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 68.0/255.0, alpha: 1.0).cgColor, UIColor.init(red: 36.0/255.0, green: 159.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor]
		view.layer.insertSublayer(backGradient, at: 0)
		
		// textview's placeholder color
		self.flatNumberTextField.attributedPlaceholder = NSAttributedString.init(string: "Flat Number:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		
		// transparent container view
		self.transparentPropertyTypeView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
		self.transparentAddressView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
		self.transparentBedroomsNumberView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
		self.transparentBathroomsNumberView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
		self.transparentKitchensNumberView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
		self.transparentOtherNumberView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
		
		if (self.addressTextView.text!.count == 0) {
			self.mapLocation.isHidden = false;
			self.mapLocation.isUserInteractionEnabled = true
		}
		else {
			self.mapLocation.isHidden = true;
			self.mapLocation.isUserInteractionEnabled = false
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		// property type
		guard let propertyRadioButton = RadioButtonView.init(frame: CGRect(x: 0, y: 0, width: self.propertyContainer.frame.size.width, height: self.propertyContainer.frame.size.height),
															 buttonLabels: self.propertyTypes,
															 selectedImage: UIImage.init(named: "selectedRadioButton")!,
															 unselectedImage: UIImage.init(named: "unselectedRadioButton")!,
															 titleColor: .white) else { return }
		propertyRadioButton.tag = propertyTypeRadioButtonTag
		propertyContainer.addSubview(propertyRadioButton)
		propertyRadioButton.delegate = self
		
		// amount of bedrooms
		guard let numberOfBedroomsView = self.view.viewWithTag(numberOfBedroomsViewTag) else { return }
		let numberOfBedroomsStepper = StepperView.init(frame: CGRect(x: 0, y: 0, width: numberOfBedroomsView.frame.size.width, height: numberOfBedroomsView.frame.size.height),
													   background: UIImage.init(named: "stepperBackground")!,
													   plusImage: UIImage.init(named: "plusButton")!,
													   minusImage: UIImage.init(named: "minusButton")!,
													   range: minNumberOfBedrooms...maxNumberOfBedrooms)
		numberOfBedroomsStepper.tag = 1;
		numberOfBedroomsView.addSubview(numberOfBedroomsStepper)
		numberOfBedroomsStepper.delegate = self
		
		// amount of bathrooms
		guard let numberOfBathroomsView = self.view.viewWithTag(numberOfBathroomsViewTag) else { return }
		let numberOfBathroomsStepper = StepperView.init(frame: CGRect(x: 0, y: 0, width: numberOfBathroomsView.frame.size.width, height: numberOfBathroomsView.frame.size.height),
													   background: UIImage.init(named: "stepperBackground")!,
													   plusImage: UIImage.init(named: "plusButton")!,
													   minusImage: UIImage.init(named: "minusButton")!,
													   range: minNumberOfBathrooms...maxNumberOfBathrooms)
		numberOfBathroomsStepper.tag = 2;
		numberOfBathroomsView.addSubview(numberOfBathroomsStepper)
		numberOfBathroomsStepper.delegate = self
		
		// amount of kitchens
		guard let numberOfKitchensView = self.view.viewWithTag(numberOfKitchensViewTag) else { return }
		let numberOfKitchensStepper = StepperView.init(frame: CGRect(x: 0, y: 0, width: numberOfKitchensView.frame.size.width, height: numberOfKitchensView.frame.size.height),
														background: UIImage.init(named: "stepperBackground")!,
														plusImage: UIImage.init(named: "plusButton")!,
														minusImage: UIImage.init(named: "minusButton")!,
														range: minNumberOfKitchens...maxNumberOfKitchens)
		numberOfKitchensStepper.tag = 3;
		numberOfKitchensView.addSubview(numberOfKitchensStepper)
		numberOfKitchensStepper.delegate = self
		
		// amount of common areas
		guard let numberOfCommonAreasView = self.view.viewWithTag(numberOfCommonAreasViewTag) else { return }
		let numberOfCommonAreasStepper = StepperView.init(frame: CGRect(x: 0, y: 0, width: numberOfCommonAreasView.frame.size.width, height: numberOfCommonAreasView.frame.size.height),
													   background: UIImage.init(named: "stepperBackground")!,
													   plusImage: UIImage.init(named: "plusButton")!,
													   minusImage: UIImage.init(named: "minusButton")!,
													   range: minNumberOfCommonAreas...maxNumberOfCommonAreas)
		numberOfCommonAreasStepper.tag = 4;
		numberOfCommonAreasView.addSubview(numberOfCommonAreasStepper)
		numberOfCommonAreasStepper.delegate = self
	}
    
    // MARK: - button handlers

	@IBAction func backPressed(_ sender: UIBarButtonItem) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func mapTapped(_ sender: UITapGestureRecognizer) {
		let center = userLocation
		let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
		let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
		let viewport = GMSCoordinateBounds.init(coordinate: northEast, coordinate: southWest)
		let config = GMSPlacePickerConfig.init(viewport: viewport)
		let placePicker = GMSPlacePickerViewController.init(config: config)
		placePicker.delegate = self
		self.navigationController?.present(placePicker, animated: true, completion: nil)
	}
	
	@IBAction func nextPressed(_ sender: UIButton) {
		self.view.endEditing(true)
		if self.addressTextView.text!.count == 0 {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: "Please enter your current address.")
			self.navigationController?.present(alert!, animated: true, completion: nil)
			return
		}
		updateUserData()
    }
	
	// MARK: - text field delegate
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		textField.placeholder = ""
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField.text! == "" {
			textField.attributedPlaceholder = NSAttributedString.init(string: "Flat Number:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		}
		self.view.endEditing(true)
	}
	
	// MARK: - Google Maps Place Picker delegate
	
	func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
		self.addressTextView.text = place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n")
		userLocation = place.coordinate
		print("user location: (\(userLocation.latitude), \(userLocation.longitude))")
		self.mapLocation.isHidden = (self.addressTextView.text.count > 0)
		self.navigationController?.dismiss(animated: true, completion: nil)
	}
	
	func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
		self.navigationController?.dismiss(animated: true, completion: {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: error.localizedDescription)
			self.navigationController?.present(alert!, animated: true, completion: nil)
		})		
	}
	
	func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
		self.navigationController?.dismiss(animated: true, completion: nil)
	}
	
	// MARK: - Core Location delegate
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		userLocation = locations[0].coordinate
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		print("FAILED UPDATE LOCATION: \(error.localizedDescription)")
	}
	
	// MARK: - radiobutton view delegate
	
	func choosedRadioButton(view: RadioButtonView, with number: Int) {
		selectedPropertyType = number
	}
	
	// MARK: - stepper view delegate
	
	func didChangeValue(_ value: Int, in view: StepperView) {
		switch view.tag {
		case 1:	numberOfBedrooms = value
		case 2:	numberOfBathrooms = value
		case 3:	numberOfKitchens = value
		case 4:	numberOfCommonAreas = value
		default:print("wrong stepper view with tag \(view.tag)")
		}
	}
	
	// MARK: - presenter methods
	
	fileprivate func updateUserData() {
		LoadingIndicatorHelper.showLoadingIndicator()
		let house: [String: Any] = ["address"			: self.addressTextView.text!,
									"flat"				: self.flatNumberTextField.text!,
									"location"			: [	"lat"	: Double(userLocation.latitude),
															"lng"	: Double(userLocation.longitude)],
									"bedrooms"			: self.numberOfBedrooms,
									"bathrooms"			: self.numberOfBathrooms,
									"kitchens"			: self.numberOfKitchens,
									"otherRooms"		: self.numberOfCommonAreas,
									"type"				: propertyTypes[selectedPropertyType].lowercased()]
		let user: [String: Any] = ["customerId"			: 0,
								   "house"				: [house],
								   "favouriteCleaners"	: [],
								   "profile"			: self.userProfile]
		self.presenter.updateUserData(body: user, isFBRegistration: self.isRegisteredWithFB, view: self)
	}
	
	// MARK: - view protocol methods
	
	func didUpdateUserData(error: NSError?) {
		LoadingIndicatorHelper.hideLoadingIndicator()
		if let serverError = error {
			let okClosure: () -> Void = {
				self.navigationController?.popToRootViewController(animated: true)
			}
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: serverError.localizedDescription, okOperations: [:], cancelOperation: ["OK": okClosure])
			self.navigationController?.present(alert!, animated: true, completion: nil)
		}
		else {
			self.navigationController?.performSegue(withIdentifier: PropertyDetailsViewController.toFinishScreenSegue, sender: self)
		}
	}
	
	// MARK: - service methods
	
	fileprivate func findUserLocation() {
		if CLLocationManager.locationServicesEnabled() {
			locationManager.delegate = self
			locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
			locationManager.startUpdatingLocation()
		}
	}
}
