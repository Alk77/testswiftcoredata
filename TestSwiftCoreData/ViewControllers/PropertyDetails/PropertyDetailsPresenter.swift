//
//  PropertyDetailsPresenter.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit
import CoreData

class PropertyDetailsPresenter: NSObject {

	func updateUserData(body: [String: Any], isFBRegistration: Bool, view: IPropertyDetailsProtocol) {
		var customerId: Int64 = 0
		var isEmailVerified = false
		let request: NSFetchRequest<Session> = Session.fetchRequest()
		let sessions = CoreDataHelper.shared.fetchObjects(request)
		if let currentSession = sessions.first {
			customerId = currentSession.customerId
			isEmailVerified = currentSession.isEmailVerified
		}		
		ServerAPIHelper.shared.updateCustomer(customerId: customerId, body: body) { (error) in
			if error == nil {
					if isEmailVerified || isFBRegistration {
						view.didUpdateUserData(error: nil)
					}
					else {
						view.didUpdateUserData(error: NSError.getError(errorCode: ServerErrorCode.NoVerifiedEmail.rawValue))
					}
			}
			else {
				view.didUpdateUserData(error: error)
			}
		}
	}
}
