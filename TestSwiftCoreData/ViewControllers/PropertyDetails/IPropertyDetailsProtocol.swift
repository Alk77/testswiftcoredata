//
//  IPropertyDetailsProtocol.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

protocol IPropertyDetailsProtocol {
	func didUpdateUserData(error: NSError?)
}
