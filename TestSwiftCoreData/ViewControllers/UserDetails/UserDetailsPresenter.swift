//
//  UserDetailsPresenter.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

class UserDetailsPresenter: NSObject {
	
	func checkPhoneNumber(_ number: String) -> Bool {
		let placeholder = "\\(\\d{3}\\)\\s\\d{3}\\-\\d{4}"
		let pattern = String(format: placeholder, number)
		
		do {
			let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
			let range = NSMakeRange(0, number.count)
			return regex.numberOfMatches(in: number, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range) > 0
		}
		catch let error {
			print("invalid regex: \(error.localizedDescription)")
			return false
		}
	}

}
