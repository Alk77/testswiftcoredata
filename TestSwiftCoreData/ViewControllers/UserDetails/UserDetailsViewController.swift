//
//  UserDetailsViewController.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

class UserDetailsViewController: UIViewController, UITextFieldDelegate, IUserDetailsProtocol {
    
    @IBOutlet weak var transparentUserView: UIView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    var userEmail:String?
	var isRegisteredWithFB = false
	
	fileprivate let presenter = UserDetailsPresenter()
	fileprivate var mainViewFrame:CGRect = CGRect.zero
	fileprivate var moveView:Bool = false
	fileprivate var keyboardIsShown:Bool = false
    
    fileprivate static let toPropertyDetailsScreenSegue = "fromUserDetailsToPropertyDetailsSegue"

    override func viewDidLoad() {
        super.viewDidLoad()

		mainViewFrame = self.view.frame
		
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: self.view.window)
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: self.view.window)
		
		initUI()
    }
	
	fileprivate func initUI() {
		// transparent navigation bar
		navigationController?.navigationBar.shadowImage = UIImage.init()
		navigationController?.navigationBar.isTranslucent = true
		navigationController?.view.backgroundColor = UIColor.clear
		navigationController?.navigationBar.backgroundColor = UIColor.clear
		
		// background
		let backGradient:CAGradientLayer = CAGradientLayer.init()
		backGradient.frame = view.bounds
		backGradient.colors = [UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 68.0/255.0, alpha: 1.0).cgColor, UIColor.init(red: 36.0/255.0, green: 159.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor]
		view.layer.insertSublayer(backGradient, at: 0)
		
		// transparent register view
		self.transparentUserView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
		
		// textfields
		self.firstNameTextField.text = ""
		self.lastNameTextField.text = ""
		self.phoneTextField.text! = ""
		self.firstNameTextField.attributedPlaceholder = NSAttributedString.init(string: "First Name:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		self.lastNameTextField.attributedPlaceholder = NSAttributedString.init(string: "Last Name:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		self.phoneTextField.attributedPlaceholder = NSAttributedString.init(string: "Phone Number:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: view.window)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: view.window)
		super.viewWillDisappear(animated)
	}
	
	// MARK: - button handlers
	
	@IBAction func backPressed(_ sender: UIBarButtonItem) {
		self.navigationController?.popViewController(animated: true)
	}

    @IBAction func nextPressed(_ sender: UIButton) {
		if self.firstNameTextField.text! == "" || self.lastNameTextField.text! == "" {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: "Please enter the full name.")
			self.navigationController?.present(alert!, animated: true, completion: nil)
		}
		else if !self.presenter.checkPhoneNumber(self.phoneTextField.text!) {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: "Please enter the valid phone number.")
			self.navigationController?.present(alert!, animated: true, completion: nil)
			self.phoneTextField.text! = ""
			self.phoneTextField.attributedPlaceholder = NSAttributedString.init(string: "Phone Number:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		}
		else {
			self.view.endEditing(true)
			self.performSegue(withIdentifier: UserDetailsViewController.toPropertyDetailsScreenSegue, sender: self)
		}
    }
    
	@IBAction func backgroundPressed(_ sender: UIControl) {
		self.view.endEditing(true)
	}
	
	// MARK: - text field delegates
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		textField.placeholder = ""
		if textField.tag == 3 {
			textField.text = "("
			if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
				moveView = true
			}
		}
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField.tag < 3 {
			let nextTextField = self.view.viewWithTag(textField.tag + 1) as! UITextField
			nextTextField.becomeFirstResponder()
		}
		else {
			self.view.endEditing(true)
		}
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		var placeholder = ""
		if textField.tag == 3 && textField.text!.count == 1 {
			textField.text! = ""
			placeholder = "Phone Number:"
		}
		else if textField.text == "" {
			if textField.tag == 1 {
				placeholder = "First Name:"
			}
			else if textField.tag == 2 {
				placeholder = "Last Name:"
			}
		}
		textField.attributedPlaceholder = NSAttributedString.init(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if textField.tag == 3 {
			let  char = string.cString(using: String.Encoding.utf8)!
			let isBackSpace = strcmp(char, "\\b")
			if (isBackSpace == -92) { //Backspace was pressed
				if textField.text!.count == 1 {
					return false
				}
				return true
			}
			if Int(string) == nil && string != "0" { // it isn't a numeric value
				return false
			}
			if range.location == 4 {
				textField.text! = textField.text! + ") "
			}
			else if range.location == 9 {
				textField.text! = textField.text! + "-"
			}
			else if textField.text!.count == 14 {
				return false
			}
		}
		return true
	}
	
	// MARK: - move the view methods
	
	@objc fileprivate func keyboardWillShow(notification:Notification) {
		if keyboardIsShown || !moveView {
			return
		}
		if let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
			var viewFrame:CGRect = view.frame
			viewFrame.origin.y -= keyboardSize.height * 0.5
			view.frame = viewFrame
			keyboardIsShown = true
		}
	}
	
	@objc fileprivate func keyboardWillHide(notification:Notification) {
		if !moveView {
			return
		}
		if let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
			var viewFrame:CGRect = view.frame
			viewFrame.origin.y += keyboardSize.height * 0.5
			view.frame = viewFrame
			
			keyboardIsShown = false
			moveView = false
		}
	}

	// MARK: navigation
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == UserDetailsViewController.toPropertyDetailsScreenSegue {
			if let vc = segue.destination as? PropertyDetailsViewController {
				let userData: [String: Any] = ["firstName"		: self.firstNameTextField.text!,
											   "lastName"		: self.lastNameTextField.text!,
											   "contactEmail"	: self.userEmail ?? "",
											   "phone"			: self.phoneTextField.text!]
				vc.userProfile = userData
				vc.isRegisteredWithFB = self.isRegisteredWithFB
			}
		}
	}
}
