//
//  LoginViewController.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit
import SpriteKit

class LoginViewController: UIViewController, ILoginProtocol, UITextFieldDelegate, ForgotPasswordPopupDelegate  {
    
    @IBOutlet weak var particleView: SKView!
    @IBOutlet weak var transparentContainerView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    fileprivate static let toRegisterScreenSegue = "fromLoginToRegisterSegue"
    fileprivate static let toFinishScreenSegue = "fromLoginToFinishSegue"
    fileprivate static let toUserDetailsScreenSegue = "fromLoginToUserDetailsSegue"
    
    fileprivate let presenter = LoginPresenter()
    fileprivate var fbEmail = ""
    fileprivate var mainViewFrame = CGRect.zero
    fileprivate var moveView = false
    fileprivate var keyboardIsShown = false

    override func viewDidLoad() {
        super.viewDidLoad()

        mainViewFrame = self.view.frame
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: self.view.window)
        
        initUI()
        tryAutoLogin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        emailTextField.text = ""
        passwordTextField.text = ""
        emailTextField.attributedPlaceholder = NSAttributedString.init(string: "Email:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        passwordTextField.attributedPlaceholder = NSAttributedString.init(string: "Password:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    fileprivate func initUI() {
		self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(),	for:.default);
		
        // transparent navigation bar
        navigationController?.navigationBar.shadowImage = UIImage.init()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = UIColor.clear
        navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        // background
        let backGradient:CAGradientLayer = CAGradientLayer.init()
        backGradient.frame = view.bounds
        backGradient.colors = [UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 68.0/255.0, alpha: 1.0).cgColor, UIColor.init(red: 36.0/255.0, green: 159.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor]
        view.layer.insertSublayer(backGradient, at: 0)
        
        // transparent container view
        self.transparentContainerView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        
        initParticles()
    }
    
    fileprivate func initParticles() {
        let scene:SKScene = SKScene.init(size: particleView.bounds.size)
        scene.scaleMode = SKSceneScaleMode.aspectFill
        particleView.allowsTransparency = true
        scene.backgroundColor = SKColor.clear
        let emitter:SKEmitterNode! = SKEmitterNode.init(fileNamed: "FCBulbParticle.sks")
        emitter.position = CGPoint.init(x: view.bounds.size.width * 0.5, y: 0)
        scene.addChild(emitter)
        particleView.presentScene(scene)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: view.window)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: view.window)
		super.viewWillDisappear(animated)
    }

    //MARK: - button handlers
    
    @IBAction func backgroundPressed(_ sender: UIControl) {
        view.endEditing(true)
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {
        if !self.presenter.checkEmail(email: self.emailTextField.text!) {
            let alert = AlertHelper.shared.showAlertView(title: "Error", message: "Please enter the valid email.")
            self.navigationController?.present(alert!, animated: true, completion: nil)
            self.emailTextField.text! = ""
            self.emailTextField.attributedPlaceholder = NSAttributedString.init(string: "Email:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
        else if !self.presenter.checkPassword(password: self.passwordTextField.text!) {
            let alert = AlertHelper.shared.showAlertView(title: "Error", message: "Password should contain from 8 to 20 alphanumeric symbols and can contain symbols !@#$%^&*-.")
            self.navigationController?.present(alert!, animated: true, completion: nil)
            self.passwordTextField.text! = ""
            self.passwordTextField.attributedPlaceholder = NSAttributedString.init(string: "Password:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
        else {
            self.login()
        }
    }
    
    @IBAction func loginFacebookPressed(_ sender: UIButton) {
        self.loginWithFacebook()
    }
    
    @IBAction func registerPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: LoginViewController.toRegisterScreenSegue, sender: self)
    }
    
    @IBAction func forgotPasswordPressed(_ sender: UIButton) {
        let forgotPasswordPopup = ForgotPasswordPopup.init(nibName: "ForgotPasswordPopup", bundle: nil)
		forgotPasswordPopup.modalPresentationStyle = .overCurrentContext
		forgotPasswordPopup.delegate = self;
		self.navigationController?.present(forgotPasswordPopup, animated: true, completion: nil)
    }
    
    // MARK: - text field delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.placeholder = ""
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone && textField.tag > 1 {
            moveView = true
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag < 2 {
            let nextTextField:UITextField = view.viewWithTag(textField.tag + 1) as! UITextField
            nextTextField.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 && textField.text == "" {
            textField.attributedPlaceholder = NSAttributedString.init(string: "Email:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
        else if textField.tag == 2 && textField.text == "" {
            textField.attributedPlaceholder = NSAttributedString.init(string: "Password:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
    }
	
	// MARK: - forgot password popup delegate
	
	func didEnterEmail(_ email: String, inView: ForgotPasswordPopup) {
		sendNewPassword(email: email)
	}
    
    // MARK: - move the view methods
    
    @objc fileprivate func keyboardWillShow(notification:Notification) {
        if keyboardIsShown || !moveView {
            return
        }
        
        if let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            var viewFrame:CGRect = view.frame
            viewFrame.origin.y -= keyboardSize.height * 0.5
            view.frame = viewFrame
            keyboardIsShown = true
        }
    }
    
    @objc fileprivate func keyboardWillHide(notification:Notification) {
        if !moveView {
            return
        }
        if let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            var viewFrame:CGRect = view.frame
            viewFrame.origin.y += keyboardSize.height * 0.5
            view.frame = viewFrame
            
            keyboardIsShown = false
            moveView = false
        }
    }
    
    // MARK: - presenter methods
    
    fileprivate func tryAutoLogin() {
        LoadingIndicatorHelper.showLoadingIndicator()
        self.presenter.tryAutoLoginInView(view: self)
    }
    
    fileprivate func login() {
        LoadingIndicatorHelper.showLoadingIndicator()
        let authentication:[String: Any] = ["type": 	"email",
                                           "email": 	self.emailTextField.text!,
                                           "password": 	self.passwordTextField.text!,
                                           "fbToken": 	NSNull.init()]
        let sessionConfig:[String: Any] = ["version": self.presenter.getApplicationVersion(),
                                           "platform": 	"ios_dev",
                                           "pushToken": NSNull.init(),
                                           "expiresIn": expiresTokenValue]
        self.presenter.login(view: self, authData: ["authentication": authentication,
                                               "sessionConfig": sessionConfig])
    }
    
    fileprivate func loginWithFacebook() {
        LoadingIndicatorHelper.showLoadingIndicator()
        presenter.loginFacebook(view: self)
    }
    
    fileprivate func sendNewPassword(email: String) {
        LoadingIndicatorHelper.showLoadingIndicator()
		presenter.sendNewPassword(to: email, view: self)
    }
    
    // MARK: - view controller protocol methods
    
    func allowAutoLogin(_ value:Bool, userRole:String?) {
        LoadingIndicatorHelper.hideLoadingIndicator()
        if value {
			self.performSegue(withIdentifier: LoginViewController.toFinishScreenSegue, sender: self)
        }
    }
    
    func completeLogin(error:Error?, userRole:String?) {
        LoadingIndicatorHelper.hideLoadingIndicator()
		if error == nil {
			self.performSegue(withIdentifier: LoginViewController.toFinishScreenSegue, sender: self)
		}
		else {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: (error?.localizedDescription)!)
            self.navigationController?.present(alert!, animated: true, completion: nil)
        }
    }
    
    func completeFacebookRegister(fbEmail:String) {
        LoadingIndicatorHelper.hideLoadingIndicator()
        self.fbEmail = fbEmail
        self.performSegue(withIdentifier: LoginViewController.toUserDetailsScreenSegue, sender: self)
    }
    
    func sendedNewPassword(error:Error?) {
        LoadingIndicatorHelper.hideLoadingIndicator()
		if let serverError = error {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: serverError.localizedDescription)
			self.navigationController?.present(alert!, animated: true, completion: nil)
		}
		else {
			let alert = AlertHelper.shared.showAlertView(title: "Password was changed", message:"Please check your email and confirm the new password.")
			self.navigationController?.present(alert!, animated: true, completion: nil)
		}
    }
    
    // MARK: - navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == LoginViewController.toUserDetailsScreenSegue {
            let vc = segue.destination as! UserDetailsViewController
            vc.userEmail = fbEmail
			vc.isRegisteredWithFB = true
        }
    }
    
}

extension UINavigationController {
	
	open override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
}
