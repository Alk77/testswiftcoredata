//
//  ILoginProtocol.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

protocol ILoginProtocol {
    func allowAutoLogin(_ value:Bool, userRole:String?)
    func completeLogin(error:Error?, userRole:String?)
    func completeFacebookRegister(fbEmail:String)
    func sendedNewPassword(error:Error?)
}
