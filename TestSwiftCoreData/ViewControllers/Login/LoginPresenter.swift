//
//  LoginPresenter.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import CoreData

class LoginPresenter: NSObject {
    
    func checkEmail(email:String) -> Bool {
        let placeholder = "^[a-z0-9A-Z]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
        let pattern = String(format: placeholder, email)
        
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
            let range = NSMakeRange(0, email.count)
            return regex.numberOfMatches(in: email, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range) > 0
        }
        catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return false
        }
    }
    
    func checkPassword(password:String) -> Bool {
        let placeholder = "^(?=.*[A-Za-z])[A-Za-z\\d\\$@!%%^\\*\\u0023\\?\\&\\.-]{8,20}$"
        let pattern = String(format: placeholder, password)
        
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
            let range = NSMakeRange(0, password.count)
            return regex.numberOfMatches(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range) > 0
        }
        catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return false
        }
    }
    
    func tryAutoLoginInView(view:ILoginProtocol) {
		let request: NSFetchRequest<Session> = Session.fetchRequest()
		let sessions = CoreDataHelper.shared.fetchObjects(request)
		guard let currentSession = sessions.first  else {
			view.allowAutoLogin(false, userRole: nil)
			return
		}
		if let refreshToken = currentSession.refreshToken,
			currentSession.isEmailVerified == true {
			let currentInterval = Date().timeIntervalSince1970
			let sessionInterval = currentSession.createdAt + currentSession.expiresIn
			if sessionInterval > Int32(currentInterval) {
				let request = self.getSessionConfig()
				ServerAPIHelper.shared.refreshToken(body: request, token: refreshToken) { (error) in
					if error != nil {
						view.allowAutoLogin(false, userRole: nil)
					}
					else {
						let request: NSFetchRequest<Session> = Session.fetchRequest()
						let sessions = CoreDataHelper.shared.fetchObjects(request)
						guard let refreshedSession = sessions.first  else {
							view.allowAutoLogin(false, userRole: nil)
							return
						}
						// update the customer
						if refreshedSession.customerId != 0 {
							ServerAPIHelper.shared.getCustomer(customerId: refreshedSession.customerId, completion: { (error) in
								if error == nil {
									view.allowAutoLogin(true, userRole: "customer")
								}
								else {
									view.allowAutoLogin(false, userRole: nil)
								}
							})
						}
						// update the cleaner
						else if refreshedSession.cleanerId != 0 {
							ServerAPIHelper.shared.getCleaner(cleanerId: refreshedSession.cleanerId, completion: { (error) in
								if error == nil {
									view.allowAutoLogin(true, userRole: "cleaner")
								}
								else {
									view.allowAutoLogin(false, userRole: nil)
								}
							})
						}
						// something wrong
						else {
							view.allowAutoLogin(false, userRole: nil)
						}
					}
				}
			}
			else {
				view.allowAutoLogin(false, userRole: nil)
			}
		}
		else {
			view.completeLogin(error: NSError.getError(errorCode: ServerErrorCode.NoVerifiedEmail.rawValue), userRole: nil)
		}
    }
    
    func login(view:ILoginProtocol, authData:Dictionary<String,Any>) {
        ServerAPIHelper.shared.login(body: authData) { (error) in
            if error != nil {
                view.completeLogin(error: error, userRole: nil)
            }
            else {
				let request: NSFetchRequest<Session> = Session.fetchRequest()
				let sessions = CoreDataHelper.shared.fetchObjects(request)
				if let currentSession = sessions.first {
					// it's an exist customer
					if currentSession.customerId != 0 {
						ServerAPIHelper.shared.getCustomer(customerId: currentSession.customerId, completion: { (error) in
							if error == nil {
								view.completeLogin(error: nil, userRole: "customer")
							}
							else {
								view.completeLogin(error: error, userRole: nil)
							}
						})
					}
						// it's an exist cleaner
					else if currentSession.cleanerId != 0 {
						ServerAPIHelper.shared.getCleaner(cleanerId: currentSession.cleanerId, completion: { (error) in
							if error == nil {
								view.completeLogin(error: nil, userRole: "cleaner")
							}
							else {
								view.completeLogin(error: error, userRole: nil)
							}
						})
					}
				}
					// something wrong
				else {
					view.completeLogin(error: NSError.getError(errorCode: ServerErrorCode.WrongRole.rawValue), userRole: nil)
				}
            }
        }
    }
    
    func loginFacebook(view:ILoginProtocol) {
        let loginManager = FBSDKLoginManager()
		loginManager.loginBehavior = .web
		
		loginManager.logIn(withReadPermissions: ["email"], from: view as? UIViewController) { (loginResult, error) in
			if error != nil {
				view.completeLogin(error: error, userRole: nil)
			}
			else if let result = loginResult {
				let fbToken = result.token
				guard let fbTokenString = fbToken?.tokenString else {
					view.completeLogin(error: NSError.getError(errorCode: ServerErrorCode.CancelledByUser.rawValue), userRole: nil)
					return
				}
				let requestMe = FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields" : "email"])
				let fbConnection = FBSDKGraphRequestConnection()
				
				// get user data from facebook
				fbConnection.add(requestMe, completionHandler: { (connect, fbResult, error) in
					if let fbError = error {
						view.completeLogin(error: fbError, userRole: nil)
					}
					else if let resultDict = fbResult as? [String: Any],
						let emailString = resultDict["email"] as? String {
						// sign in with server side
						let authentication = ["type" : 		"fb",
											  "email": 		emailString,
											  "password" : 	"",
											  "fbToken" :	fbTokenString]
						ServerAPIHelper.shared.signin(body: ["authentication" : authentication, "sessionConfig" : self.getSessionConfig()], completion: { (error) in
							if let serverError = error {
								view.completeLogin(error: serverError, userRole: nil)
							}
							else {
								let request: NSFetchRequest<Session> = Session.fetchRequest()
								let sessions = CoreDataHelper.shared.fetchObjects(request)
								if let currentSession = sessions.first {
									// it's a new customer
									if currentSession.isNewUser == true {
										ServerAPIHelper.shared.getCustomer(customerId: currentSession.customerId, completion: { (error) in
											if error == nil {
												view.completeFacebookRegister(fbEmail: emailString)
											}
											else {
												view.completeLogin(error: error, userRole: nil)
											}
										})
									}
									// it's an exist customer
									else if currentSession.customerId != 0 {
										ServerAPIHelper.shared.getCustomer(customerId: currentSession.customerId, completion: { (error) in
											if error == nil {
												view.completeLogin(error: nil, userRole: "customer")
											}
											else {
												view.completeLogin(error: error, userRole: nil)
											}
										})
									}
									// it's an exist cleaner
									else if currentSession.cleanerId != 0 {
										ServerAPIHelper.shared.getCleaner(cleanerId: currentSession.cleanerId, completion: { (error) in
											if error == nil {
												view.completeLogin(error: nil, userRole: "cleaner")
											}
											else {
												view.completeLogin(error: error, userRole: nil)
											}
										})
									}
										// something wrong
									else {
										view.completeLogin(error: NSError.getError(errorCode: ServerErrorCode.WrongRole.rawValue), userRole: nil)
									}
								}
							}
						})
					}
					else {
						view.completeLogin(error: NSError.getError(errorCode: ServerErrorCode.NoEmailPermission.rawValue), userRole: nil)
					}
				})
				fbConnection.start()
			}
		}
	}
	
	func sendNewPassword(to email: String, view: ILoginProtocol) {
		ServerAPIHelper.shared.resetPassword(email: email) { (error) in
			view.sendedNewPassword(error: error)
		}
	}
	
	// MARK: - service methods
    
    func getApplicationVersion() -> String {
        let infoDictionary = Bundle.main.infoDictionary
        if let majorVersion:String = infoDictionary?["CFBundleShortVersionString"] as? String,
            let minorVersion:String = infoDictionary?["CFBundleVersion"] as? String {
            return String(format: "V%@b%@", majorVersion, minorVersion)
        }
        else {
            return ""
        }
    }

	fileprivate func getSessionConfig() -> [String: Any] {
		return ["version" :		self.getApplicationVersion(),
				"platform" :	"ios_dev",
				"pushToken" :	NSNull.init(),
				"expiresIn" :	expiresTokenValue]
	}
}
