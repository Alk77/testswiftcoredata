//
//  FinishViewController.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit
import CoreData

class FinishViewController: UIViewController, IFinishProtocol {
    
    @IBOutlet weak var transparentUserDetailsView: UIView!
	@IBOutlet weak var customerStackView: UIStackView!
	@IBOutlet weak var houseStackView: UIStackView!
	@IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
	
	fileprivate let presenter = FinishPresenter()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		initUI()
    }
	
	fileprivate func initUI() {
		// transparent navigation bar
		navigationController?.navigationBar.shadowImage = UIImage.init()
		navigationController?.navigationBar.isTranslucent = true
		navigationController?.view.backgroundColor = UIColor.clear
		navigationController?.navigationBar.backgroundColor = UIColor.clear
		
		// remove back button
		self.navigationItem.hidesBackButton = true
		
		// background
		let backGradient:CAGradientLayer = CAGradientLayer.init()
		backGradient.frame = view.bounds
		backGradient.colors = [UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 68.0/255.0, alpha: 1.0).cgColor, UIColor.init(red: 36.0/255.0, green: 159.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor]
		view.layer.insertSublayer(backGradient, at: 0)
		
		// transparent container view
		self.transparentUserDetailsView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
	}

    override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		let request: NSFetchRequest<Session> = Session.fetchRequest()
		let sessions = CoreDataHelper.shared.fetchObjects(request)
		if let currentSession = sessions.first {
			if currentSession.customerId != 0 {
				fillCustomerInfo()
			}
			else {
				fillCleanerInfo()
			}
		}
		else {
			customerStackView.removeFromSuperview()
			houseStackView.removeFromSuperview()
			let okClosure: () -> Void = {
				self.navigationController?.popToRootViewController(animated: true)
			}
			let message = "Something went wrong. Please try to relogin."
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: message, okOperations: [:], cancelOperation: ["OK": okClosure])
			self.navigationController?.present(alert!, animated: true, completion: nil)
		}
    }
	
	// MARK: - button handlers
	
	@IBAction func logOutPressed(_ sender: UIButton) {
		logout()
	}
	
	// MARK: - presenter methods
	
	fileprivate func logout() {
		LoadingIndicatorHelper.showLoadingIndicator()
		self.presenter.logout(view: self)
	}
	
	// MARK: - view controller delegate methods
	
	func finishedLogout(error: Error?) {
		LoadingIndicatorHelper.hideLoadingIndicator()
		if let serverError = error {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: serverError.localizedDescription)
			self.navigationController?.present(alert!, animated: true, completion: nil)
		}
		else {
			self.navigationController?.popToRootViewController(animated: true)
		}
	}
	
	// MARK: - service methods
	
	fileprivate func fillCustomerInfo() {
		let customerRequest: NSFetchRequest<Customer> = Customer.fetchRequest()
		let customers = CoreDataHelper.shared.fetchObjects(customerRequest)
		if let customer = customers.first {
			for view in self.customerStackView.subviews {
				if let label = view as? UILabel {
					switch label.tag {
					case 1:
						label.text! = "Customer:"
					case 2:
						label.text! = label.text! + " " + (customer.profile?.firstName ?? "")
					case 3:
						label.text! = label.text! + " " + (customer.profile?.lastName ?? "")
					case 4:
						label.text! = label.text! + " " + (customer.profile?.phone ?? "")
					case 5:
						label.text! = label.text! + " " + (customer.profile?.contactEmail ?? "")
					default:
						print("wrong label tag \(label.tag) in customer section")
					}
				}
			}
		}
		else {
			houseStackView.removeFromSuperview()
			viewHeightConstraint.constant = UIDevice.current.userInterfaceIdiom == .phone ? 300 : 400
		}
		let houseRequest: NSFetchRequest<House> = House.fetchRequest()
		let houses = CoreDataHelper.shared.fetchObjects(houseRequest)
		if let house = houses.first {
			for view in self.houseStackView.subviews {
				if let label = view as? UILabel {
					switch label.tag {
					case 1:
						label.text! = "House:"
					case 2:
						label.text! = label.text! + " " + (house.type ?? "")
					case 3:
						let address = house.address?.replacingOccurrences(of: "\n", with: ", ")
						label.text! = label.text! + " " + (address ?? "")
					case 4:
						label.text! = label.text! + " " + (house.flat ?? "none")
					case 5:
						label.text! = label.text! + " " + String(house.bedrooms)
					case 6:
						label.text! = label.text! + " " + String(house.bathrooms)
					case 7:
						label.text! = label.text! + " " + String(house.kitchens)
					case 8:
						label.text! = label.text! + " " + String(house.otherRooms)
					default:
						print("wrong label tag \(label.tag) in house section")
					}
				}
			}
		}
		else {
			houseStackView.removeFromSuperview()
			viewHeightConstraint.constant = UIDevice.current.userInterfaceIdiom == .phone ? 300 : 400
		}
	}
	
	fileprivate func fillCleanerInfo() {
		houseStackView.removeFromSuperview()
		viewHeightConstraint.constant = UIDevice.current.userInterfaceIdiom == .phone ? 300 : 400
		let cleanerRequest: NSFetchRequest<Cleaner> = Cleaner.fetchRequest()
		let cleaners = CoreDataHelper.shared.fetchObjects(cleanerRequest)
		if let cleaner = cleaners.first {
			for view in self.customerStackView.subviews {
				if let label = view as? UILabel {
					switch label.tag {
					case 1:
						label.text! = "Cleaner:"
					case 2:
						label.text! = label.text! + " " + (cleaner.profile?.firstName ?? "")
					case 3:
						label.text! = label.text! + " " + (cleaner.profile?.lastName ?? "")
					case 4:
						label.text! = label.text! + " " + (cleaner.profile?.phone ?? "")
					case 5:
						label.text! = label.text! + " " + (cleaner.profile?.contactEmail ?? "")
					default:
						print("wrong label tag \(label.tag) in cleaner section")
					}
				}
			}
		}
	}
	
}
