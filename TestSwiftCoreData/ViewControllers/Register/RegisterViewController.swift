//
//  RegisterViewController.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate, IRegisterProtocol {
    
    @IBOutlet weak var transparentRegisterView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    fileprivate static let toUserDetailsScreenSegue = "fromRegisterToUserDetailsSegue"
	
	fileprivate let presenter = RegisterPresenter()
	fileprivate var mainViewFrame = CGRect.zero
	fileprivate var moveView = false
	fileprivate var keyboardIsShown = false
	var isRegisteredWithFB = false
	
    
    override func viewDidLoad() {
        super.viewDidLoad()

		mainViewFrame = self.view.frame
		
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: self.view.window)
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: self.view.window)
		
		initUI()
    }
	
	fileprivate func initUI() {
		// transparent navigation bar
		navigationController?.navigationBar.shadowImage = UIImage.init()
		navigationController?.navigationBar.isTranslucent = true
		navigationController?.view.backgroundColor = UIColor.clear
		navigationController?.navigationBar.backgroundColor = UIColor.clear
		
		// background
		let backGradient:CAGradientLayer = CAGradientLayer.init()
		backGradient.frame = view.bounds
		backGradient.colors = [UIColor.init(red: 19.0/255.0, green: 19.0/255.0, blue: 68.0/255.0, alpha: 1.0).cgColor, UIColor.init(red: 36.0/255.0, green: 159.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor]
		view.layer.insertSublayer(backGradient, at: 0)
		
		// transparent register view
		self.transparentRegisterView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
		
		// textfields
		emailTextField.text = ""
		passwordTextField.text = ""
		self.confirmPasswordTextField.text! = ""
		emailTextField.attributedPlaceholder = NSAttributedString.init(string: "Email:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		passwordTextField.attributedPlaceholder = NSAttributedString.init(string: "Password:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		self.confirmPasswordTextField.attributedPlaceholder = NSAttributedString.init(string: "Confirm Password:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: view.window)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: view.window)
		super.viewWillDisappear(animated)
	}
	
    // MARK: button handlers
	
	@IBAction func backPressed(_ sender: UIBarButtonItem) {
		self.navigationController?.popViewController(animated: true)
	}
	
    @IBAction func nextPressed(_ sender: UIButton) {
		if !self.presenter.checkEmail(email: self.emailTextField.text!) {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: "Please enter the valid email.")
			self.navigationController?.present(alert!, animated: true, completion: nil)
			self.emailTextField.text! = ""
			self.emailTextField.attributedPlaceholder = NSAttributedString.init(string: "Email:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		}
		else if !self.presenter.checkPassword(password: self.passwordTextField.text!) {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: "Password should contain from 6 to 20 alphanumeric symbols and can contain symbols !@#$%^&*-.")
			self.navigationController?.present(alert!, animated: true, completion: nil)
			self.passwordTextField.text! = ""
			self.passwordTextField.attributedPlaceholder = NSAttributedString.init(string: "Password:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		}
		else if self.passwordTextField.text! != self.confirmPasswordTextField.text! {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: "The password does not match the one you entered earlier.")
			self.navigationController?.present(alert!, animated: true, completion: nil)
			self.confirmPasswordTextField.text! = ""
			self.confirmPasswordTextField.attributedPlaceholder = NSAttributedString.init(string: "Confirm Password:", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		}
		else {
			self.view.endEditing(true)
			register()
		}
    }
	
	@IBAction func backgroundPressed(_ sender: UIControl) {
		self.view.endEditing(true)
	}
	
	// MARK: text field delegates
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		textField.placeholder = ""
		if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone && textField.tag == 3 {
			moveView = true
		}
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField.tag < 3 {
			let nextTextField = self.view.viewWithTag(textField.tag + 1) as! UITextField
			nextTextField.becomeFirstResponder()
		}
		else {
			self.view.endEditing(true)
		}
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField.text == "" {
			var placeholder = ""
			if textField.tag == 1 {
				placeholder = "Email:"
			}
			else if textField.tag == 2 {
				placeholder = "Password:"
			}
			else {
				placeholder = "Confirm Password:"
			}
			textField.attributedPlaceholder = NSAttributedString.init(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
		}
	}
	
	// MARK: - move the view methods
	
	@objc fileprivate func keyboardWillShow(notification:Notification) {
		if keyboardIsShown || !moveView {
			return
		}		
		if let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
			var viewFrame:CGRect = view.frame
			viewFrame.origin.y -= keyboardSize.height * 0.5
			view.frame = viewFrame
			keyboardIsShown = true
		}
	}
	
	@objc fileprivate func keyboardWillHide(notification:Notification) {
		if !moveView {
			return
		}
		if let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
			var viewFrame:CGRect = view.frame
			viewFrame.origin.y += keyboardSize.height * 0.5
			view.frame = viewFrame
			
			keyboardIsShown = false
			moveView = false
		}
	}
	
	// MARK: - presenter methods
	
	fileprivate func register() {
		LoadingIndicatorHelper.showLoadingIndicator()
		let authentication:[String: Any] = ["type": "email",
											"email": self.emailTextField.text!,
											"password": self.passwordTextField.text!,
											"fbToken": NSNull.init()]
		self.presenter.register(view: self, registrationData: authentication)
	}
	
	// MARK: - view controller protocol methods
	
	func completeRegister(error: Error?) {
		LoadingIndicatorHelper.hideLoadingIndicator()
		if error == nil {
			self.performSegue(withIdentifier: RegisterViewController.toUserDetailsScreenSegue, sender: self)
		}
		else {
			let alert = AlertHelper.shared.showAlertView(title: "Error", message: (error?.localizedDescription)!)
			self.navigationController?.present(alert!, animated: true, completion: nil)
		}
	}
}
