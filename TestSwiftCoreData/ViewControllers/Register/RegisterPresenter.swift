//
//  RegisterPresenter.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/4/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

class RegisterPresenter: NSObject {
	
	func checkEmail(email:String) -> Bool {
		let placeholder = "^[a-z0-9A-Z]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
		let pattern = String(format: placeholder, email)
		
		do {
			let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
			let range = NSMakeRange(0, email.count)
			return regex.numberOfMatches(in: email, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range) > 0
		}
		catch let error {
			print("invalid regex: \(error.localizedDescription)")
			return false
		}
	}
	
	func checkPassword(password:String) -> Bool {
		let placeholder = "^(?=.*[A-Za-z])[A-Za-z\\d\\$@!%%\\*\\u0023\\?\\&\\.-]{8,20}$"
		let pattern = String(format: placeholder, password)
		
		do {
			let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
			let range = NSMakeRange(0, password.count)
			return regex.numberOfMatches(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range) > 0
		}
		catch let error {
			print("invalid regex: \(error.localizedDescription)")
			return false
		}
	}
	
	func register(view: IRegisterProtocol, registrationData: [String: Any]) {
		ServerAPIHelper.shared.register(body: registrationData) { (error) in
			view.completeRegister(error: error)
		}
	}

}
