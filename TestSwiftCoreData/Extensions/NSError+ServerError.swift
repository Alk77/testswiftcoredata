//
//  NSError+ServerError.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/5/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import Foundation

enum ServerErrorCode: Int {
    // server codes
    case WrongData = 400
    case AccessDenied = 401
    case WrongRole = 403
    case UserNotFound = 404
    case AlreadyRegistered = 409
    case EmailNotVerifiedByServer = 422
    case ServerError = 500
    
    // local codes
    case NoInternetConnection = 3000
    case CancelledByUser = 3001
    case NoEmailPermission = 3002
    case NoVerifiedEmail = 3003
    case WrongAddress = 3004
    case NoAvailableCleaners = 3005
    case CannotSaveAvatarImage = 3006
    case GoogleZeroResults = 3007
    case GoogleOverQueryLimit = 3008
    case GoogleRequestDenied = 3009
    case GoogleInvalidRequest = 3010
    case GoogleUnknownError = 3011
    case UnderConstuctionFeature = 3012
}

extension NSError {   
    
    class func getErrorMessage(errorCode:Int, uiCode:Int) -> String {
        let errorMessage:String
        
        switch errorCode {
        // server codes
        case ServerErrorCode.WrongData.rawValue: // 400
            if uiCode == 1000 {
                errorMessage = "Wrong sended data. Empty email."
            }
            else if uiCode == 1001 {
                errorMessage = "Wrong sended data. Invalid email."
            }
            else if uiCode == 1002 {
                errorMessage = "Wrong sended data. Empty password."
            }
            else if uiCode == 1003 {
                errorMessage = "Wrong sended data. Invalid password."
            }
            else if uiCode == 1004 {
                errorMessage = "Wrong sended data. You can order a cleaner for at least one hour from the current time."
            }
            else {
                errorMessage = "Wrong sended data."
            }
        case ServerErrorCode.AccessDenied.rawValue: // 401
            if uiCode == 1000 {
                errorMessage = "Access denied. Your password doesn't match."
            }
            else if uiCode == 1001 {
                errorMessage = "Access denied. Wrong authentication token."
            }
            else if uiCode == 1002 {
                errorMessage = "Access denied. Wrong Facebook token."
            }
            else if uiCode == 1003 {
                errorMessage = "Access denied. This account is not registered."
            }
            else if uiCode == 1004 {
                errorMessage = "Access denied. This account was deleted by admin."
            }
            else if uiCode == 1005 {
                errorMessage = "Access denied. This account is not approved by admin."
            }
            else {
                errorMessage = "Access denied. It's not your account or password doesn't match."
            }
        case ServerErrorCode.WrongRole.rawValue: // 403
            errorMessage = "Access denied. You're trying to get access to cleaner or user account with wrong current role."
        case ServerErrorCode.UserNotFound.rawValue: // 404
            errorMessage = "User data not found."
        case ServerErrorCode.AlreadyRegistered.rawValue: // 409
            if uiCode == 1000 {
                errorMessage = "Sorry, user with this email already registered but not verified. Check your email and confirm the registration."
            }
            else if uiCode == 1001 {
                errorMessage = "Sorry, user with this email already registered and verified. Try to use other email address."
            }
            else if uiCode == 1002 {
                errorMessage = "Sorry, seems we have crash of the database. Try to log in later."
            }
            else if uiCode == 1003 {
                errorMessage = "Sorry, seems Facebook didn't provide your email address. Try to register with email and password."
            }
            else if uiCode == 1004 {
                errorMessage = "Sorry, seems you registered by Facebook. Try to login with your Facebook account."
            }
            else if uiCode == 1005 {
                errorMessage = "Sorry, user with this email already registered by Facebook. Try to login with your Facebook account."
            }
            else if uiCode == 1006 {
                errorMessage = "Sorry, this interval already reserved. Try to choose other time or cleaner."
            }
            else if uiCode == 1007 {
                errorMessage = "Sorry, you try to accept the booking which status isn't pending."
            }
            else if uiCode == 1008 {
                errorMessage = "Sorry, you try to accept the booking which already rejected by admin."
            }
            else if uiCode == 1009 {
                errorMessage = "Sorry, seems we have error on the Stripe side. Try to book again."
            }
            else if uiCode == 1010 {
                errorMessage = "Sorry, we have mistakes between client and server payment data. Please relaunch application and try to book again."
            }
            else {
                errorMessage = "Sorry, some data between the client and server parts do not match. Please relaunch application and try again."
            }
        case ServerErrorCode.EmailNotVerifiedByServer.rawValue: // 422
            errorMessage = "Your email not verified. Please check your email box, find the email we sended and go to link in this email. After that you should to relogin."
        case ServerErrorCode.ServerError.rawValue: // 500
            errorMessage = "Internal server error."
            
        // local codes
        case ServerErrorCode.NoInternetConnection.rawValue:
            errorMessage = "No internet connection."
        case ServerErrorCode.CancelledByUser.rawValue:
            errorMessage = "Cancelled by user."
        case ServerErrorCode.NoEmailPermission.rawValue:
            errorMessage = "You couldn't login by Facebook because you have no email permission."
        case ServerErrorCode.NoVerifiedEmail.rawValue:
            errorMessage = "Your email not verified. Please check your email box, find the email we sended and go to link in this email. After that you should to relogin."
        case ServerErrorCode.WrongAddress.rawValue:
            errorMessage = "We cannot to find this address. Please pick it on the map"
        case ServerErrorCode.NoAvailableCleaners.rawValue:
            errorMessage = "Sorry, no available cleaners for this time in this area."
        case ServerErrorCode.CannotSaveAvatarImage.rawValue:
            errorMessage = "Sorry, unable to save the avatar image"
        case ServerErrorCode.GoogleZeroResults.rawValue:
            errorMessage = "Sorry, cannot find your postal code. Please check it and try again."
        case ServerErrorCode.GoogleOverQueryLimit.rawValue:
            errorMessage = "Sorry, for today the number of requests for the Google server is exhausted. Try it tomorrow."
        case ServerErrorCode.GoogleRequestDenied.rawValue:
            errorMessage = "Sorry, this request is denied by Google. Try it later."
        case ServerErrorCode.GoogleInvalidRequest.rawValue:
            errorMessage = "Invalid request. Seems the Google has changed its API. You may need a new version of the application."
        case ServerErrorCode.GoogleUnknownError.rawValue:
            errorMessage = "Unknown Google server error. Try it again."
        case ServerErrorCode.UnderConstuctionFeature.rawValue:
            errorMessage = "Unfortunately, this function is still in development. We will implement it later."
        default:
            errorMessage = "Unknown error."
        }
        return errorMessage
    }
    
    class func getErrorFromServerError(error:NSError?) -> NSError? {
        return error
    }
    
    class func getError(errorCode:Int) -> NSError {
        return NSError.init(domain: "TestSwiftCoreData", code: errorCode, userInfo: [NSLocalizedDescriptionKey: NSError.getErrorMessage(errorCode: errorCode, uiCode: 0)])
    }
    
    class func getError(errorCode:Int, uiCode:Int) -> NSError {
        return NSError.init(domain: "TestSwiftCoreData", code: errorCode, userInfo: [NSLocalizedDescriptionKey: NSError.getErrorMessage(errorCode: errorCode, uiCode: uiCode)])
    }
}
