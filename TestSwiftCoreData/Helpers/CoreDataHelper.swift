//
//  CoreDataHelper.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 12/19/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit
import Sync

class CoreDataHelper: NSObject {
	
	static let shared = CoreDataHelper()
	let dataStack: DataStack
	
	private override init() {
		self.dataStack = DataStack(modelName: "TestSwiftCoreData")
		super.init()
		applicationDocumentsDirectory()
	}
	
	func fetchObjects<RES: NSFetchRequestResult, REQ: NSFetchRequest<RES>>(_ request: REQ) -> [RES] {
		request.returnsObjectsAsFaults = false
		return try! self.dataStack.viewContext.fetch(request)
	}
	
	private func applicationDocumentsDirectory() {
		if let url = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).last {
			print(url.absoluteString)
		}
	}

}
