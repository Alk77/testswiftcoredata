//
//  ServerAPIHelper.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/6/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit
import Alamofire
import Sync

class ServerAPIHelper: NSObject {
    
//    var currentSession:Session?
//    var customer:Customer?
    
    fileprivate static let mainURL = "http://la-cleaner.us-west-1.elasticbeanstalk.com/v1/application/"
    fileprivate static let signinURL = ServerAPIHelper.mainURL + "session/signin"
    fileprivate static let registerURL = ServerAPIHelper.mainURL + "session/register"
    fileprivate static let loginURL = ServerAPIHelper.mainURL + "session/login"
    fileprivate static let refreshTokenURL = ServerAPIHelper.mainURL + "session/refresh"
	fileprivate static let resetPasswordURL = ServerAPIHelper.mainURL + "email/send/set_random_password"
	fileprivate static let logoutURL = ServerAPIHelper.mainURL + "session/logout"
    fileprivate static let getCustomerURL = ServerAPIHelper.mainURL + "customer/"
    fileprivate static let updateCustomerURL = ServerAPIHelper.mainURL + "customer/"
	fileprivate static let getCleanerURL = ServerAPIHelper.mainURL + "cleaner/"
    static let shared = ServerAPIHelper()
    
    // MARK: - session methods
    
    func signin(body: Parameters, completion:@escaping ((NSError?) -> Void)) {
        Alamofire.request(ServerAPIHelper.signinURL,
                          method: .post,
                          parameters: body,
                          encoding: URLEncoding.queryString,
                          headers: nil).responseData { response in
                            switch response.result {
                            case .success(let value):
								do {
									if let json = try JSONSerialization.jsonObject(with: value, options: [JSONSerialization.ReadingOptions.mutableContainers]) as? [String: Any] {
										print(json)
										CoreDataHelper.shared.dataStack.sync([json], inEntityNamed: Session.entity().name!) { error in
											if error == nil {
												completion(nil)
											}
											else {
												completion(error)
											}
										}
									}
								}
								catch let error {
									completion(error as NSError)
								}
                            case .failure(let error):
                                completion(error as NSError)
                            }
        }
    }

    func register(body: Parameters, completion:@escaping ((NSError?) -> Void)) {
        Alamofire.request(ServerAPIHelper.registerURL,
                          method: .post,
                          parameters: body,
                          encoding: URLEncoding.queryString,
                          headers: nil).responseData { response in
                            switch response.result {
                            case .success(let value):
								do {
									if let json = try JSONSerialization.jsonObject(with: value, options: [JSONSerialization.ReadingOptions.mutableContainers]) as? [String: Any] {
										print(json)
										CoreDataHelper.shared.dataStack.sync([json], inEntityNamed: Session.entity().name!) { error in
											if error == nil {
												completion(nil)
											}
											else {
												completion(error)
											}
										}
									}
								}
								catch let error {
									completion(error as NSError)
								}
							case .failure(let error):
                                completion(error as NSError)
                            }
        }
    }
    
    func login(body: Parameters, completion:@escaping ((NSError?) -> Void)) {
        Alamofire.request(ServerAPIHelper.loginURL,
                          method: .post,
                          parameters: body,
                          encoding: URLEncoding.queryString,
                          headers: nil).responseData { response in
                            switch response.result {
                            case .success(let value):
                                do {
                                    if let json = try JSONSerialization.jsonObject(with: value, options: [JSONSerialization.ReadingOptions.mutableContainers]) as? [String: Any] {
										print(json)
										CoreDataHelper.shared.dataStack.sync([json], inEntityNamed: Session.entity().name!) { error in
											if error == nil {
												completion(nil)
											}
											else {
												completion(error)
											}
										}
									}
                                }
                                catch let error {
                                    completion(error as NSError)
                                }
                            case .failure(let error):
                                completion(error as NSError)
                            }
        }
    }

    
	func refreshToken(body: Parameters, token: String, completion:@escaping ((NSError?) -> Void)) {
		Alamofire.request(ServerAPIHelper.refreshTokenURL,
						  method: .post,
						  parameters: body,
						  encoding: URLEncoding.queryString,
						  headers: ["X-Refresh-Token": token]).responseData { response in
							switch response.result {
							case .success(let value):
								do {
									if let json = try JSONSerialization.jsonObject(with: value, options: [JSONSerialization.ReadingOptions.mutableContainers]) as? [String: Any] {
										print(json)
										CoreDataHelper.shared.dataStack.sync([json], inEntityNamed: Session.entity().name!) { error in
											if error == nil {
												completion(nil)
											}
											else {
												completion(error)
											}
										}
									}
								}
								catch let error {
									completion(error as NSError)
								}
							case .failure(let error):
								completion(error as NSError)
							}
		}
	}
	
	func resetPassword(email: String, completion:@escaping ((NSError?) -> Void)) {
		Alamofire.request(ServerAPIHelper.resetPasswordURL,
						  method: .post,
						  parameters: ["email": email],
						  encoding: URLEncoding.queryString,
						  headers: nil).responseData { (response) in
							switch response.result {
							case .success:
								completion(nil)
							case .failure(let error):
								completion(error as NSError)
							}
		}
	}
	
	func logout(completion:@escaping ((NSError?) -> Void)) {
		Alamofire.request(ServerAPIHelper.logoutURL,
						  method: .delete,
						  parameters: nil,
						  encoding: URLEncoding.queryString,
						  headers: nil).responseData { (response) in
							switch response.result {
							case .success:
								let request: NSFetchRequest<Session> = Session.fetchRequest()
								let sessions = CoreDataHelper.shared.fetchObjects(request)
								var isCleaner = false
								if let currentSession = sessions.first {
									isCleaner = (currentSession.cleanerId != 0)
								}
								// clear Core Data
								CoreDataHelper.shared.dataStack.sync([], inEntityNamed: Session.entity().name!, operations: .delete, completion: { (error) in
									if let sessionError = error {
										completion(sessionError as NSError)
									}
									else {
										if isCleaner {
											CoreDataHelper.shared.dataStack.sync([], inEntityNamed: Cleaner.entity().name!, operations: .delete, completion: { (error) in
												completion(error as NSError?)
											})
										}
										else {
											CoreDataHelper.shared.dataStack.sync([], inEntityNamed: Customer.entity().name!, operations: .delete, completion: { (error) in
												completion(error as NSError?)
											})
										}
									}
								})
							case .failure(let error):
								completion(error as NSError)
							}
		}
	}
    
    // MARK: - customer methods
    
    func getCustomer(customerId: Int64, completion:@escaping ((NSError?) -> Void)) {
		Alamofire.request(ServerAPIHelper.getCustomerURL + String(customerId),
						  method: .get,
						  parameters: nil,
						  encoding: URLEncoding.queryString,
						  headers: nil).responseData { (response) in
							switch response.result {
							case .success(let value):
								do {
									if let json = try JSONSerialization.jsonObject(with: value, options: [JSONSerialization.ReadingOptions.mutableContainers]) as? [String: Any] {
										print(json)
										CoreDataHelper.shared.dataStack.sync([json], inEntityNamed: Customer.entity().name!) { error in
											if error == nil {
												completion(nil)
											}
											else {
												completion(error)
											}
										}
									}
								}
								catch let error {
									completion(error as NSError)
								}
							case .failure(let error):
								completion(error as NSError)
							}
		}
    }
	
	func updateCustomer(customerId: Int64, body: Parameters, completion:@escaping ((NSError?) -> Void)) {
		Alamofire.request(ServerAPIHelper.updateCustomerURL + String(customerId),
						  method: .put,
						  parameters: body,
						  encoding: URLEncoding.queryString,
						  headers: nil).responseData { (response) in
							switch response.result {
							case .success:
								completion(nil)
							case .failure(let error):
								completion(error as NSError)
							}
		}
	}
	
	// MARK: - cleaner methods
	
	func getCleaner(cleanerId: Int64, completion:@escaping ((NSError?) -> Void)) {
		Alamofire.request(ServerAPIHelper.getCleanerURL + String(cleanerId),
						  method: .get,
						  parameters: nil,
						  encoding: URLEncoding.queryString,
						  headers: nil).responseData { (response) in
							switch response.result {
							case .success(let value):
								do {
									if let json = try JSONSerialization.jsonObject(with: value, options: [JSONSerialization.ReadingOptions.mutableContainers]) as? [String: Any] {
										print(json)
										CoreDataHelper.shared.dataStack.sync([json], inEntityNamed: Cleaner.entity().name!) { error in
											if error == nil {
												completion(nil)
											}
											else {
												completion(error)
											}
										}
									}
								}
								catch let error {
									completion(error as NSError)
								}
							case .failure(let error):
								completion(error as NSError)
							}
		}
	}

}
