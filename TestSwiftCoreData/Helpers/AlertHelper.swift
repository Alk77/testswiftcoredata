//
//  AlertHelper.swift
//  TestSwiftCoreData
//
//  Created by Alex Korabl'ov on 9/5/18.
//  Copyright © 2018 mastercheck. All rights reserved.
//

import UIKit

class AlertHelper: NSObject {
    
    static let shared = AlertHelper()
    var isShowAlertView:Bool
    
    private override init() {
        isShowAlertView = false
    }
    
    func showAlertView(title:String, message:String) -> UIAlertController? {
        if isShowAlertView {
            return nil
        }
        isShowAlertView = true
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let cancelAction:UIAlertAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
            self.isShowAlertView = false
        })
        alertController.addAction(cancelAction)
        
        return alertController
    }
    
    func showAlertView(title:String, message:String, okOperations:Dictionary<String, () -> Void>) -> UIAlertController? {
        if isShowAlertView {
            return nil
        }
        isShowAlertView = true
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        for (buttonTitle, buttonOperation) in okOperations {
            let action = UIAlertAction.init(title: buttonTitle, style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.isShowAlertView = false
                buttonOperation()
            }
            alertController.addAction(action)
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            self.isShowAlertView = false
        }
        alertController.addAction(cancelAction)
        
        return alertController
    }
	
	func showAlertView(title:String, message:String, okOperations:Dictionary<String, () -> Void>, cancelOperation:Dictionary<String, () -> Void>) -> UIAlertController? {
		if isShowAlertView {
			return nil
		}
		isShowAlertView = true
		
		let alertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
		for (okTitle, okOperation) in okOperations {
			let action = UIAlertAction.init(title: okTitle, style: UIAlertAction.Style.default) { (UIAlertAction) in
				self.isShowAlertView = false
				okOperation()
			}
			alertController.addAction(action)
		}
		
		if cancelOperation.count > 1 {
			fatalError("Should be only one cancel operation!")
		}
		for (cancelTitle, cancel) in cancelOperation {
			let cancelAction = UIAlertAction.init(title: cancelTitle, style: UIAlertAction.Style.cancel) { (UIAlertAction) in
				self.isShowAlertView = false
				cancel()
			}
			alertController.addAction(cancelAction)
		}
		
		return alertController
	}
}
